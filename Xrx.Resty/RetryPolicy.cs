﻿using System;
using System.Collections.Generic;
using System.Net;
using Xrx.Resty.Abstractions;

namespace Xrx.Resty
{
    public class RetryPolicy : IRetryPolicy
    {
        private Dictionary<Type, int> exceptionPolicies;
        private Dictionary<HttpStatusCode, int> statusPolicies;

        public RetryPolicy()
        {
            exceptionPolicies = new Dictionary<Type, int>();
            statusPolicies = new Dictionary<HttpStatusCode, int>();
        }

        public IRetryPolicy On<T>(int retries)
        {
            exceptionPolicies.Add(typeof(T), retries);
            return this;
        }

        public IRetryPolicy On(HttpStatusCode status, int retries)
        {
            statusPolicies.Add(status, retries);
            return this;
        }

        public virtual RetryEligibility ValidateResponse<T>(IWebResponse<T> response)
        {
            RetryEligibility eligibility = new RetryEligibility();
            Type responseException = response.Exception?.GetType();
            Type baseException = typeof(Exception);
            if (statusPolicies.ContainsKey(response.StatusCode) &&
                statusPolicies[response.StatusCode]-- > 0)
            {
                eligibility.IsEligible = true;
                eligibility.NeedsAuthorization = response.StatusCode == HttpStatusCode.Unauthorized;
            }
            else
            {
                eligibility.IsEligible = (exceptionPolicies.ContainsKey(baseException) &&
                                          exceptionPolicies[baseException]-- > 0) ||
                                         (exceptionPolicies.ContainsKey(responseException) &&
                                          exceptionPolicies[responseException]-- > 0);
            }
            return eligibility;
        }
    }
}
