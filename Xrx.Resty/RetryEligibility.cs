﻿namespace Xrx.Resty
{
    public class RetryEligibility
    {
        public bool IsEligible { get; set; }
        public bool NeedsAuthorization { get; set; }
    }
}
