using System;
using Xrx.Resty.Abstractions;
using Xrx.Resty.Structures;

namespace Xrx.Resty
{
    public class WebRequest : IWebRequest
    {
        public string RelativePath { get; set; }
        public DataSet Headers { get; set; } = new DataSet();
        public DataSet QueryParams { get; set; } = new DataSet();
        public TimeSpan Timeout { get; set; } = TimeSpan.FromSeconds(10);
        public object Body { get; set; }
        public IRetryPolicy RetryPolicy { get ; set ; }
    }
}