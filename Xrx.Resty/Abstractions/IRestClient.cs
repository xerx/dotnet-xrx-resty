﻿using System;

namespace Xrx.Resty.Abstractions
{
    public interface IRestClient : IDisposable
    {
        string BasePath { get; set; }
        IRestClientProxy StartRequest(string relativePath);
        IRestClientBase SetRequest(IWebRequest request);
    }
}
