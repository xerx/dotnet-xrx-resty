﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace Xrx.Resty.Abstractions
{
    public interface IRetryPolicy
    {
        IRetryPolicy On<T>(int retries);
        IRetryPolicy On(HttpStatusCode status, int retries);
        RetryEligibility ValidateResponse<T>(IWebResponse<T> response);
    }
}