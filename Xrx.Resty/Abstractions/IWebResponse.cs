﻿using System;
using System.Net;

namespace Xrx.Resty.Abstractions
{
    public interface IWebResponse<T>
    {
        HttpStatusCode StatusCode { get; set; }
        bool Success { get; set; }
        Exception Exception { get; set; }
        T Data { get; set; }
    }
}
