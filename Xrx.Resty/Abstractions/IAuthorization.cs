﻿using System;
namespace Xrx.Resty.Abstractions
{
    public interface IAuthorization
    {
        void ApplyTo(IWebRequest webRequest);
    }
}
