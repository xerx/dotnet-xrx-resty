﻿using System;
using System.Threading.Tasks;

namespace Xrx.Resty.Abstractions
{
    public interface IAuthorizer
    {
        Task<IAuthorization> UpdateAuthorization();
        void Authorize(IWebRequest request);
    }
}
