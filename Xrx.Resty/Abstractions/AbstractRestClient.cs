﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xrx.Resty.Structures;

namespace Xrx.Resty.Abstractions
{
    public abstract class AbstractRestClient : IRestClient, IRestClientProxy
    {
        public abstract string BasePath { get; set; }

        protected IDataParser parser;
        protected IAuthorizer authorizer;
        protected Queue<IWebRequest> requestQueue;
        protected object queueLock = new { };

        protected delegate Task<IWebResponse<T>> RestMethod<T>(IWebRequest request, CancellationToken cancellationToken);

        private static Task<IWebResponse<T>> FailedRequestTask<T>() =>
            Task.FromResult<IWebResponse<T>>(new WebResponse<T>
            {
                Exception = new NullReferenceException("Request cannot be found")
            });

        protected AbstractRestClient(IDataParser parser)
        {
            this.parser = parser;
            requestQueue = new Queue<IWebRequest>();
        }
        protected AbstractRestClient(IDataParser parser,
                                  IAuthorizer authorizer)
        {
            this.parser = parser;
            this.authorizer = authorizer;
            requestQueue = new Queue<IWebRequest>();
        }
        public IRestClientBase SetRequest(IWebRequest request)
        {
            lock (queueLock)
            {
                requestQueue.Enqueue(request);
            }
            return this;
        }
        #region Fluent API
        public IRestClientProxy StartRequest(string relativePath)
        {
            lock (queueLock)
            {
                requestQueue.Enqueue(new WebRequest
                {
                    RelativePath = relativePath
                });
            }
            return this;
        }
        public IRestClientProxy WithHeaders(DataSet headers)
        {
            lock (requestQueue)
            {
                requestQueue.Peek().Headers = headers;
            }
            return this;
        }

        public IRestClientProxy WithQueryParams(DataSet parameters)
        {
            lock (requestQueue)
            {
                requestQueue.Peek().QueryParams = parameters;
            }
            return this;
        }

        public IRestClientProxy WithBody(object body)
        {
            lock (requestQueue)
            {
                requestQueue.Peek().Body = body;
            }
            return this;
        }

        public IRestClientProxy WithRetryPolicy(IRetryPolicy policy)
        {
            lock (requestQueue)
            {
                requestQueue.Peek().RetryPolicy = policy;
            }
            return this;
        }

        public IRestClientProxy WithTimeout(TimeSpan timeSpan)
        {
            lock (requestQueue)
            {
                requestQueue.Peek().Timeout = timeSpan;
            }
            return this;
        }
        #endregion

        protected virtual async Task<IWebResponse<T>> InvokeRestMethod<T>(RestMethod<T> method,
                                                                          IWebRequest request,
                                                                          CancellationToken cancellationToken)
        {
            authorizer?.Authorize(request);
            cancellationToken.ThrowIfCancellationRequested();

            IWebResponse<T> response = await method(request, cancellationToken);

            cancellationToken.ThrowIfCancellationRequested();
            if(!response.Success && request.RetryPolicy != null)
            {
                RetryEligibility eligibility = request.RetryPolicy.ValidateResponse(response);
                if(eligibility.IsEligible)
                {
                    if(eligibility.NeedsAuthorization)
                    {
                        await authorizer.UpdateAuthorization();
                        cancellationToken.ThrowIfCancellationRequested();
                        authorizer.Authorize(request);
                    }
                    cancellationToken.ThrowIfCancellationRequested();
                    return await InvokeRestMethod(method, request, cancellationToken);
                }
            }
            return response;
        }
        private IWebRequest DequeueRequest()
        {
            try
            {
                lock (requestQueue)
                {
                    return requestQueue.Dequeue();
                }
            }
            catch
            {
                return null;
            }
        }
        public Task<IWebResponse<T>> Get<T>(CancellationToken cancellationToken = default(CancellationToken))
        {
            IWebRequest request = DequeueRequest();
            if (request == null) { return FailedRequestTask<T>(); }

            cancellationToken.ThrowIfCancellationRequested();
            return InvokeRestMethod<T>(InternalGet<T>, request, cancellationToken);
        }
        public Task<IWebResponse<T>> Post<T>(CancellationToken cancellationToken = default(CancellationToken))
        {
            IWebRequest request = DequeueRequest();
            if (request == null) { return FailedRequestTask<T>(); }

            cancellationToken.ThrowIfCancellationRequested();
            return InvokeRestMethod<T>(InternalPost<T>, request, cancellationToken);
        }
        public Task<IWebResponse<T>> Put<T>(CancellationToken cancellationToken = default(CancellationToken))
        {
            IWebRequest request = DequeueRequest();
            if (request == null) { return FailedRequestTask<T>(); }

            cancellationToken.ThrowIfCancellationRequested();
            return InvokeRestMethod<T>(InternalPut<T>, request, cancellationToken);
        }
        public Task<IWebResponse<T>> Delete<T>(CancellationToken cancellationToken = default(CancellationToken))
        {
            IWebRequest request = DequeueRequest();
            if (request == null) { return FailedRequestTask<T>(); }

            cancellationToken.ThrowIfCancellationRequested();
            return InvokeRestMethod<T>(InternalDelete<T>, request, cancellationToken);
        }

        public virtual void Dispose()
        {
            requestQueue.Clear();
            requestQueue = null;
        }

        protected abstract Task<IWebResponse<T>> InternalGet<T>(IWebRequest request, CancellationToken cancellationToken = default(CancellationToken));
        protected abstract Task<IWebResponse<T>> InternalPost<T>(IWebRequest request, CancellationToken cancellationToken = default(CancellationToken));
        protected abstract Task<IWebResponse<T>> InternalPut<T>(IWebRequest request, CancellationToken cancellationToken = default(CancellationToken));
        protected abstract Task<IWebResponse<T>> InternalDelete<T>(IWebRequest request, CancellationToken cancellationToken = default(CancellationToken));
    }
}
