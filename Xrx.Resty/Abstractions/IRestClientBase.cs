﻿using System.Threading;
using System.Threading.Tasks;

namespace Xrx.Resty.Abstractions
{
    public interface IRestClientBase
    {
        Task<IWebResponse<T>> Get<T>(CancellationToken cancellationToken = default(CancellationToken));
        Task<IWebResponse<T>> Post<T>(CancellationToken cancellationToken = default(CancellationToken));
        Task<IWebResponse<T>> Put<T>(CancellationToken cancellationToken = default(CancellationToken));
        Task<IWebResponse<T>> Delete<T>(CancellationToken cancellationToken = default(CancellationToken));
    }
}
