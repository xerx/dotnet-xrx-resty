﻿using System;
namespace Xrx.Resty.Abstractions
{
    public interface IDataParser
    {
        string Serialize(object @object);
        T Deserialize<T>(string data);
    }
}
