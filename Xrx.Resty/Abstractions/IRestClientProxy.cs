﻿using System;
using Xrx.Resty.Structures;

namespace Xrx.Resty.Abstractions
{
    public interface IRestClientProxy : IRestClientBase
    {
        IRestClientProxy WithHeaders(DataSet headers);
        IRestClientProxy WithQueryParams(DataSet parameters);
        IRestClientProxy WithBody(object body);
        IRestClientProxy WithRetryPolicy(IRetryPolicy policy);
        IRestClientProxy WithTimeout(TimeSpan timeSpan);
    }
}
