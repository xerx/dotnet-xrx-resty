﻿using System;
using Xrx.Resty.Structures;

namespace Xrx.Resty.Abstractions
{
    public interface IWebRequest
    {
        string RelativePath { get; set; }
        DataSet Headers { get; set; }
        DataSet QueryParams { get; set; }
        object Body { get; set; }

        TimeSpan Timeout { get; set; }
        IRetryPolicy RetryPolicy { get; set; }
    }
}
