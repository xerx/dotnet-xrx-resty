using System;
using System.Net;
using Xrx.Resty.Abstractions;

namespace Xrx.Resty
{
    public class WebResponse<T> : IWebResponse<T>
    {
        public HttpStatusCode StatusCode { get; set; }
        public T Data { get; set; }
        public Exception Exception { get; set; }

        private bool? success;
        public virtual bool Success
        {
            get => success ?? Exception == null && (int)StatusCode / 100 == 2;
            set => success = value;
        }
    }
}