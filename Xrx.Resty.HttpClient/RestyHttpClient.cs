﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Xrx.Resty.Abstractions;

namespace Xrx.Resty.HttpClient
{
    public class RestyHttpClient : AbstractRestClient
    {
        public RestyHttpClient(IDataParser parser) : base(parser)
        {
        }

        public RestyHttpClient(IDataParser parser, IAuthorizer authorizer) : base(parser, authorizer)
        {
        }

        public override string BasePath { get; set; }

        protected override Task<IWebResponse<T>> InternalDelete<T>(IWebRequest request, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        protected override Task<IWebResponse<T>> InternalGet<T>(IWebRequest request, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        protected override Task<IWebResponse<T>> InternalPost<T>(IWebRequest request, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        protected override Task<IWebResponse<T>> InternalPut<T>(IWebRequest request, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }
    }
}
