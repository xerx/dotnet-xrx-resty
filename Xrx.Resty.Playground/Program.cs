﻿using System;
using System.Security.Policy;
using System.Threading;
using Xrx.Resty.Abstractions;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Diagnostics;

namespace Xrx.Resty.Playground
{
    class MainClass
    {
        public static async Task Main(string[] args)
        {
            IRestClient r = new RC(new JsonParser());
            Call(r);
            //Call2(r);
            //Call(r);
            //Call2(r);
            //Call(r);

            await Task.Delay(63000);
        }
        private static async Task Call(IRestClient r)
        {
            var pol = new RetryPolicy().On<Exception>(2);
            var u = await r.StartRequest("api/users").WithRetryPolicy(pol).Get<RS>();
            Console.WriteLine(u.Success);
            Console.WriteLine(new JsonParser().Serialize(u.Data));
        }
        private static async Task Call2(IRestClient r)
        {
            var u = await r.StartRequest("api/users/contacts?userId=1").Get<RS>();
            Console.WriteLine(new JsonParser().Serialize(u.Data));
        }
    }
    class RS
    {
        public int Type { get; set; }
        
        public string Message { get; set; }
    }
    class RC : AbstractRestClient, IAuthorizer
    {
        private HttpClient http;
        public RC(IDataParser dataParser) : base(dataParser)
        {
            http = new HttpClient();
            http.BaseAddress = new Uri(BasePath);
        }

        public override string BasePath { get; set; } = "https://private-7b30d2-edwards1.apiary-mock.com/";

        public void Authorize(IWebRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<IAuthorization> UpdateAuthorization()
        {
            throw new NotImplementedException();
        }

        protected override Task<IWebResponse<T>> InternalDelete<T>(IWebRequest request, CancellationToken cancellationToken = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        protected override async Task<IWebResponse<T>> InternalGet<T>(IWebRequest request, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var t = await http.GetAsync(request.RelativePath);
                var f = await t.Content.ReadAsStringAsync();
                var m = parser.Deserialize<T>(f);
                return new WebResponse<T>
                {
                    Data = m,
                    StatusCode = t.StatusCode
                };
            }
            catch(Exception ee)
            {
                Debug.WriteLine(ee.Message);
                return new WebResponse<T>
                {
                    Exception = ee
                };
            }
        }

        protected override Task<IWebResponse<T>> InternalPost<T>(IWebRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        protected override Task<IWebResponse<T>> InternalPut<T>(IWebRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}