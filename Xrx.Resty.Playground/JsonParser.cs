﻿using Newtonsoft.Json;
using Xrx.Resty.Abstractions;

namespace Xrx.Resty.Playground
{
    public class JsonParser : IDataParser
    {
        public T Deserialize<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }

        public string Serialize(object @object)
        {
            return JsonConvert.SerializeObject(@object);
        }
    }
}
